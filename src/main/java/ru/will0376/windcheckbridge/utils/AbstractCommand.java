package ru.will0376.windcheckbridge.utils;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public abstract class AbstractCommand {
	String commandName;

	public AbstractCommand(String commandName) {
		this.commandName = commandName;
	}

	public static String getPermissionPrefix() {
		return "wind.";
	}

	public abstract List<Argument> getArgList();

	public CommandLine getLine(String[] args) throws Exception {
		return new DefaultParser().parse(getArguments(), args);
	}

	public abstract ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception;

	public String getPermission() {
		return getCommandName();
	}

	public String getUsage() {
		List<Argument> argMap = getArgList();
		if (argMap != null) return argMap.stream()
				.filter(e -> !e.isHide())
				.map(e -> e.getName() + " - " + e.getDesc())
				.collect(Collectors.joining("\n"));
		return "";
	}

	public Argument.ArgumentBuilder getArgBuilder(String name) {
		return Argument.builder().name(name);
	}

	public List<String> getTabList(String[] args, ICommandSender sender) {
		if (args.length >= 2) {
			List<Argument> argMap = getArgList();
			if (argMap != null) return argMap.stream()
					.filter(e -> !e.isHide())
					.map(e -> "-" + e.getName())
					.collect(Collectors.toList());
		}
		return Collections.singletonList("");
	}

	public ActionStructure.ActionStructureBuilder getStartedAction() {
		return ActionStructure.builder().command(getCommandName());
	}

	public Options getArguments() {
		Options options = new Options();
		if (getArgList() != null) for (Argument argument : getArgList()) {
			Option build = Option.builder(argument.getName().replace("-", ""))
					.desc(argument.getDesc())
					.required(argument.isRequired())
					.hasArg(argument.isHasArg())
					.build();
			options.addOption(build);
		}
		return options;
	}

	public MinecraftServer getServer() {
		return FMLCommonHandler.instance().getMinecraftServerInstance();
	}

	@Builder(toBuilder = true)
	@Getter
	public static class Argument {
		@Builder.Default
		public boolean hasArg = false;
		String name;
		String desc;
		@Builder.Default
		boolean hide = false;
		@Builder.Default
		boolean required = false;

		public static class ArgumentBuilder {
			public ArgumentBuilder setHasArg() {
				hasArg(true);
				return this;
			}

			public ArgumentBuilder setRequired() {
				required(true);
				return this;
			}

			public ArgumentBuilder setHide() {
				hide(true);
				return this;
			}
		}
	}

	@Builder(toBuilder = true)
	@Data
	public static class ActionStructure {
		String adminNick;
		String command;
		@Builder.Default
		String reason = "";
		@Builder.Default
		String[] args = new String[]{};

		@Builder.Default
		long time = System.currentTimeMillis();

		@Builder.Default
		boolean canceled = false;

		public ActionStructure setCanceled() {
			canceled = true;
			reason = "Unknown error";
			return this;
		}

		public ActionStructure setCanceled(String reason) {
			this.reason = reason;
			this.canceled = true;
			return this;
		}
	}
}
