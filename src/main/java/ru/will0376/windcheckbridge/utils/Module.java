package ru.will0376.windcheckbridge.utils;

import lombok.Builder;
import lombok.Data;
import net.minecraftforge.fml.common.Loader;

@Data
@Builder(toBuilder = true)
public class Module {
	String name;
	String modid;
	String version;
	String prefix;
	String moduleName;
	@Builder.Default
	boolean enabled = true;

	public boolean isLoaded() {
		return Loader.isModLoaded(modid);
	}

	public static class ModuleBuilder {
		public ModuleBuilder setDefaultNames(String in) {
			name(in);
			prefix(in);
			moduleName(in);
			return this;
		}
	}
}
