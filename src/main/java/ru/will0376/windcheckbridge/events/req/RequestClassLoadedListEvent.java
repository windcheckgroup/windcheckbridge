package ru.will0376.windcheckbridge.events.req;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.Token;

@Getter
@AllArgsConstructor
public class RequestClassLoadedListEvent extends Event {
	String targetNick;
	String adminNick;
	Token.Requester requester;
	String[] options;
}
