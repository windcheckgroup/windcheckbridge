package ru.will0376.windcheckbridge.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;

@Getter
@AllArgsConstructor
public class ParseAnswerFromClientEvent extends Event {
	String message;
	String ip;
}
