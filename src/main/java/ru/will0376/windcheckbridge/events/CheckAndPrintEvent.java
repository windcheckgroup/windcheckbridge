package ru.will0376.windcheckbridge.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.Token;

import java.awt.image.BufferedImage;

@Getter
@AllArgsConstructor
public class CheckAndPrintEvent extends Event {
	Token token;
	String text;
	String clickText;
	BufferedImage image;

	public CheckAndPrintEvent(Token token, String text, String clickText) {
		this.token = token;
		this.text = text;
		this.clickText = clickText;
	}
}
