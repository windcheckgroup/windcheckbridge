package ru.will0376.windcheckbridge;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import ru.will0376.windcheckbridge.events.RegisterModuleEvent;
import ru.will0376.windcheckbridge.utils.AVersion;
import ru.will0376.windcheckbridge.utils.Module;

import static ru.will0376.windcheckbridge.WindCheckBridge.*;

@Mod(modid = MOD_ID, name = MOD_NAME, version = VERSION, acceptedMinecraftVersions = "[1.12.2]", acceptableRemoteVersions = "*")
public class WindCheckBridge {
	public static final String MOD_ID = "windcheckbridge";
	public static final String MOD_NAME = "WindCheckBridge";
	public static final String VERSION = "@version@";
	public static final boolean debug = true;
	public static final Module module = Module.builder().modid(MOD_ID).version(VERSION).setDefaultNames(MOD_NAME).build();
	public static AVersion aVersion;

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new RegisterModuleEvent(module));
	}
}
