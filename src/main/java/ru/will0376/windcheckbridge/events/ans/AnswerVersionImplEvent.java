package ru.will0376.windcheckbridge.events.ans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.AVersion;
import ru.will0376.windcheckbridge.utils.Module;

@Getter
@AllArgsConstructor
public class AnswerVersionImplEvent extends Event {
	AVersion aVersionImpl;
	Module module;
}
