package ru.will0376.windcheckbridge.utils;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Event;

import java.io.File;

public abstract class AVersion {
	public abstract void printErrorToSender(ICommandSender sender, String text);

	public abstract void printToSender(ICommandSender sender, String text);

	public abstract void printToSenderWithClick(ICommandSender sender, String text, String textClick);

	public abstract void printToSenderWithCase(ICommandSender sender, String text);

	public abstract boolean senderCanUseCommand(ICommandSender sender, String text);

	public abstract SubCommands registerNewCommand(AbstractCommand commandAbstract);

	public abstract Token.Requester registerModule(Module module);

	public abstract <T extends Event> boolean sendEvent(T event);

	public abstract String getRandomKey();

	public abstract EntityPlayerMP findPlayer(String in);

	public abstract EntityPlayerMP findPlayer(String in, boolean bool);

	public abstract boolean checkPlayer(String in, boolean bool);

	public abstract Token createNewToken(String playerNick, String adminNick, Token.Requester requester);

	public abstract File getDefaultDir();

	public abstract boolean isModuleLoaded(String moduleModId);

	public abstract Module getLoadedModule(String moduleModId);

	public abstract Module getModuleModidAll();

	public abstract Token.Requester registerNewRequester(String name);

	public abstract boolean isRequesterExist(String name);

}
