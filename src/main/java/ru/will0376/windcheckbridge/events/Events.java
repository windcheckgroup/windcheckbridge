package ru.will0376.windcheckbridge.events;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.events.ans.AnswerVersionImplEvent;

@Mod.EventBusSubscriber
public class Events {
	@SubscribeEvent
	public static void event(AnswerVersionImplEvent event) {
		if (event.getModule().equals(WindCheckBridge.module)) WindCheckBridge.aVersion = event.getAVersionImpl();
	}
}
