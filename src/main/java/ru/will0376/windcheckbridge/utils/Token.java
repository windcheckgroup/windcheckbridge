package ru.will0376.windcheckbridge.utils;

import lombok.Builder;
import lombok.Getter;
import ru.will0376.windcheckbridge.WindCheckBridge;

@Builder(toBuilder = true)
@Getter
public class Token {
	String playerNick;
	Requester requester;
	String adminNick;
	String base64Token;
	@Builder.Default
	boolean toDelete = false;

	public void setDeleted() {
		toDelete = true;
	}

	public Token generateAndSetB64Token() {
		base64Token = StringXORer.encode(String.format("%s,%s,%s,%s", playerNick, requester.name(), adminNick, System.currentTimeMillis()), WindCheckBridge.aVersion
				.getRandomKey());
		return this;
	}

	@Getter
	public enum Requester {
		Admin,
		Console
	}

}
