package ru.will0376.windcheckbridge.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.Module;

@AllArgsConstructor
@Getter
public class ReloadEvent extends Event {
	boolean moduleOnly;
	Module module;
	String adminNick;
}
