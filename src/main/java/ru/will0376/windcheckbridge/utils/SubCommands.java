package ru.will0376.windcheckbridge.utils;

import lombok.Getter;

@Getter
public enum SubCommands {
	;
	AbstractCommand comma;

	SubCommands(AbstractCommand comma) {
		this.comma = comma;
	}
}
