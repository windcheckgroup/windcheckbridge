package ru.will0376.windcheckbridge.events.ans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.Token;

import java.awt.image.BufferedImage;

@AllArgsConstructor
@Getter
public class SaveImageEvent extends Event {
	Token token;
	BufferedImage image;
}
