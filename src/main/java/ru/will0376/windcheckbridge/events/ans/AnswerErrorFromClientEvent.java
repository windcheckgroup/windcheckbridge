package ru.will0376.windcheckbridge.events.ans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;

@Getter
@AllArgsConstructor
public class AnswerErrorFromClientEvent extends Event {
	String adminNick;
	String link;
	String playerNick;
}
