package ru.will0376.windcheckbridge.events.req;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.Token;

@Cancelable
@Getter
@AllArgsConstructor
@Setter
public class RequestLogFromPlayerEvent extends Event {
	String targetNick;
	String adminNick;
	Token.Requester requester;

	boolean debug;
}
