package ru.will0376.windcheckbridge.events.ans;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;

@Getter
@AllArgsConstructor
public class AnswerClassLoadedListEvent extends Event {
	String adminNick;
	JsonObject jo;
	String playerNick;
}
