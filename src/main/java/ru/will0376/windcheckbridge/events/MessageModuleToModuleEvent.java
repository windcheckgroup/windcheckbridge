package ru.will0376.windcheckbridge.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.minecraftforge.fml.common.eventhandler.Event;
import ru.will0376.windcheckbridge.utils.Module;

@Data
@AllArgsConstructor
public class MessageModuleToModuleEvent extends Event {
	Module from;
	Module to;
	String action;
	String data;
}
