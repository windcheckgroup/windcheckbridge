package ru.will0376.windcheckbridge.events;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraftforge.fml.common.eventhandler.Event;

@Getter
@AllArgsConstructor
public class ParseJOToAnotherRequester extends Event {
	String adminNick;
	JsonObject jo;
	String playerNick;
}
